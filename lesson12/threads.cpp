#include "threads.h"

std::mutex mtx;

void I_Love_Threads() {
	std::cout << "i love threads\n";
}

void call_I_Love_Threads() {
	std::thread t(I_Love_Threads);
	t.join();
}

void getPrimes(int begin, int end, std::vector<int>& primes) {
	bool isPrime = false;
	for (int i = begin; i <= end; i++) {
		for (int j = 2; j < i; j++) {
			if (i % j == 0) {
				isPrime = true;
				break;
			}
		}
		if (!isPrime) {
			primes.push_back(i);
		}
		isPrime = false;
	}
}

void printVector(std::vector<int> primes) {
	for (unsigned int i = 0; i < primes.size(); i++) {
		std::cout << primes[i] << std::endl;
	}
}

std::vector<int> callGetPrimes(int begin, int end) {
	std::vector<int> primes;
	clock_t start = clock();
	std::thread t(getPrimes, begin, end, ref(primes));
	t.join();
	std::cout << "the amount of time for the thread to end: " <<
		(clock() - start) / CLOCKS_PER_SEC << std::endl;
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file) {
	bool isPrime = false;
	for (int i = begin; i <= end; i++) {
		for (int j = 2; j < sqrt(i); j++) {
			if (i%j == 0) {
				isPrime = true;
				break;
			}
		}
		if (!isPrime) {
			mtx.lock();
			file << i << endl;
			mtx.unlock();
		}
		isPrime = false;
	}
}


void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N) {
	std::vector<std::thread> v;
	std::ofstream file;
	while (true) {
		file.open(filePath);
		if (file.is_open()) {
			break;
		}
	}
	int adder = end / N;
	int start = begin;
	clock_t curr = clock();
	for (int i = 0; i < N; i++) {
		v.push_back(std::thread(writePrimesToFile, start, start + adder, ref(file)));
		start += adder;
	}
	std::vector<std::thread>::iterator it;
	for (it = v.begin(); it != v.end(); ++it) {
		it->join();
	}
	std::cout << "total time: " << (clock() - curr) / CLOCKS_PER_SEC << " seconds" << std::endl;
	file.close();
}