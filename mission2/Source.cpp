#pragma warning(disable: 4996)
#include "MessagesSender.h"
enum choices{SIGN_IN = 1,SIGN_OUT,PRINT_USERS,EXIT};

void getUserName(char buff[]) {
	std::cout << "Enter user name: ";
	fgets(buff, NAME_LEN, stdin); // using char* for the name
	buff[strlen(buff) - 1] = '\0'; // deleting the '\n'
}


int main() { 
	MessagesSender msgr("admin");
	char userName[NAME_LEN] = {};
	FILE* file; // defining struct 
	std::thread (msgr.readTextFile).detach();
	std::thread (msgr.send_messages).detach();
	int choice;
	bool flag = true;
	while (flag) {
		std::cout << "Welcome!\n"
			<< "1. you can sign in by user name\n"
			<< "2. you can sign out\n"
			<< "3. print all the logged in users\n"
			<< "4. exit\n"
			<< "your choice: ";
		std::cin >> choice;
		getchar();
		switch (choice)
		{
			case SIGN_IN:
				getUserName(userName);
				msgr.signin(std::string(userName,strlen(userName)));
				file = fopen("data.txt", "a");
				fprintf(file, "%s is now logged in\n", userName);
				fclose(file);
				break;
			case SIGN_OUT:
				getUserName(userName);
				msgr.signout(std::string(userName, strlen(userName)));
				file = fopen("data.txt", "a");
				fprintf(file, "%s just logged out\n", userName);
				fclose(file);
				break;
			case PRINT_USERS:
				msgr.connected_users();
				break;
			case EXIT:
				flag = false;
				break;
			default:
				std::cout << "invalid choice" << std::endl;
				break;
		}
		system("pause");
		system("cls");
	}
	getchar();
	return 0;
}