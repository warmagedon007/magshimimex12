#define NAME_LEN 100

#include <iostream>
#include <thread>
#include <mutex>
#include <set>
#include <algorithm>
#include <string>
#include <Windows.h>
#include <queue>
#include <fstream>

class MessagesSender {

public:
	MessagesSender(std::string admin);
	MessagesSender();
	~MessagesSender();

	void signin(std::string userName);
	void signout(std::string userName);
	void connected_users() const;
	static void readTextFile();
	static void send_messages();
};
