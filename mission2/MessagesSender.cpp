#pragma warning(disable: 4996)
#include "MessagesSender.h"

std::set<std::string> users;
std::queue<std::string> msg;

std::mutex msg_mtx;
std::condition_variable msg_con;

MessagesSender::MessagesSender(std::string admin) {
	users.insert(admin);
}

MessagesSender::MessagesSender() {
	MessagesSender("Admin");
}

MessagesSender::~MessagesSender() {}

void MessagesSender::signin(std::string userName) {
	if (users.count(userName)) {
		std::cout << "\nthe user is already signed in\n" << std::endl;
		return;
	}
	users.insert(userName);
	std::cout << "\nthe user name " << userName << " is logged in" << std::endl;
}

void MessagesSender::signout(std::string userName) {
	if (!users.count(userName)) {
		std::cout << "\nthe user " << userName << " is not signed in" << std::endl;
		return;
	}
	users.erase(userName);
	std::cout << "\nthe user name " << userName << " is disconnected" << std::endl;
}

void MessagesSender::connected_users() const {
	std::set<std::string>::iterator it;
	for (it = users.begin(); it != users.end(); ++it) {
		std::cout << *it << std::endl;
	}
}

void MessagesSender::readTextFile() {
	std::ifstream file;
	std::string line;
	FILE* f = NULL;
	file.open("data.txt");
	while (true) {
		std::unique_lock<std::mutex> locker(msg_mtx);
		std::cout << "\nscanning the file \n";
		while (std::getline(file, line)) {
			if (line.size() == 0) {
				break;
			}
			msg.push(line);
		}
		locker.unlock();
		msg_con.notify_one();
		file.close();
		f = fopen("data.txt", "w");
		fprintf(f, ""); fclose(f); 
		std::cout << "\nlength of msg = " << msg.size() << "\n";
		Sleep(60000);
	}
}

void MessagesSender::send_messages() {
	std::ofstream file;
	file.open("output.txt");
	std::set<std::string>::iterator it;
	std::string line;
	while (true) {
		std::unique_lock<std::mutex> locker(msg_mtx);
		msg_con.wait(locker);
		std::cout << "sending messages to file" << std::endl;
		while (!msg.empty()) {
			for (it = users.begin(); it != users.end(); ++it) {
				if (msg.empty()) {
					break;
					locker.unlock();
				}
				file << "to " << *it << " :" << msg.front() << "\n";
				std::cout << "to " << *it << " :" << msg.front() << "\n";
			}
			msg.pop();
		}
		file.close();
	}
}

